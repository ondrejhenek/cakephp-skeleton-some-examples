# Canes.cz CakePHP boilerplate
 
My custom skeleton and templates used in my CakePHP apps.

## Skeleton contains:

+ Basic CSS I use nearly everywhere (no resets included!)
+ Basic JavaScript for all sorts of things. Because you can always delete stuff.
+ Czech localization with .pot files included

## Template contains:

+ Modified session messages for better i18n
+ Enhanced session messages with custom CSS for better *success* and *failure* recognition

# Canes.cz funny examples and stuff

## Simple Czech - Spanish vocabulary

+ Cakey AJAX examples
+ not-at-all-ugly CSS :)
+ Search (coming soon)

## Basic Google maps API v3 web app

+ it has menu?

### ToDo:
+ add contextual features - based on location show some easter eggs