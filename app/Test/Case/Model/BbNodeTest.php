<?php
App::uses('BbNode', 'Model');

/**
 * BbNode Test Case
 *
 */
class BbNodeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.bb_node'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->BbNode = ClassRegistry::init('BbNode');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->BbNode);

		parent::tearDown();
	}

}
