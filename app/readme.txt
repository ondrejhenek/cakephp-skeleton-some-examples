=================== ToDo ===================
- funkční ajax s json inputem
- pagination se změnou URL
- přidávání položek (nerefresh celého obsahu)

================== SLOVNÍČEK ==================
- editace položek ve vlastním okně
- vyhledávání (komplexnější)

================= STROJ ČASU ==================
- futuristic font
- pozadí podobné WolframAlpha s 900x600 texturou do ztracena (obvody & čipy)
- posunuli jste se o 5 vteřin do budoucnosti (a nic zajímavého se nestalo)
- za tu dobu byste:
	zhubli x kalorií přemýšlením
	2x vydechli a nadechli
	přenesli 10^-90 atlatského oceánu v kávové lžičce přes most nad hořící lávou
	ujeli 5 metrů (při rychlosti 1 metr za skundu)
	obletěli zeměkouli při rychlosti (xxx)
	snědli asi 1/10^5 velryby
	vypočítali 1/xxx tinu odpovědi na otázku Co je smyslem

================= Version Checker ==================
- bude parsovat weby a hledat čísla verzí. Každý si zadá své číslo, to se pak porovná a bude email/Rss notifikace


FOXIT READER