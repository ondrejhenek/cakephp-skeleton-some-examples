<?php $this->layout = ''; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Skvělý web s Google Maps</title>
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
	<link rel="stylesheet" type="text/css" href="/css/bb_css.css" />
	<script type="text/javascript" src="/js/bb_js.js"></script>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyChjR2_quW7eSdgkiN0Xf-SDIlIGUkGUfE&sensor=true"></script>
	<script>

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-6149741-3']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

	</script>
</head>
<body onload="initialize()">

	<div id="map_canvas"></div>

	<header>
		<nav class="mainMenu">
			<a href="#"><h1>Google Maps web</h1></a>
			<a href="#">Domů</a>
			<a href="#">Druhý odkaz</a>
			<a href="#">Další</a>
			<a href="#">A ještě něco</a>
		</nav>
	</header>
		
	<footer>
	</footer>
</body>
</html>
