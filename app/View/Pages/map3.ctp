<?php $this->layout = ''; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title><?php echo $title_for_layout; ?></title>
	<meta name="viewport" content="initial-scale=1.0, user-scalable=no">
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyChjR2_quW7eSdgkiN0Xf-SDIlIGUkGUfE&sensor=true"></script>
	<?php
		echo $this->Html->meta('icon');
		echo $this->Html->css(array('bb_css2'));
		echo $this->Html->script(array('infobox-1.1.12.min', 'jquery-1.9.1.min', 'bb_js2'));
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>

	<script>

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-6149741-3']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

	</script>
	
</head>
<body onload="initialize()">

	<div id="map_canvas"></div>
	
	<?php echo $this->Session->flash(); ?>

	<header>
		<nav class="mainMenu">
			<a href="#"><h1>Google Maps web</h1></a>
			<a href="#">Domů</a>
			<a href="#">Druhý odkaz</a>
			<a href="#">Další</a>
			<a href="#">A ještě něco</a>
		</nav>
	</header>
		
	<footer class="mainFoot">
	</footer>
	
	<?php echo $this->element('sql_dump');?>
	
<div id="shadowContent" style="display: none;">
	<div id="newNodeFormShadow">
		<?php echo $this->Form->create('BbNode', array('url' => array('controller' => 'bbnodes', 'action' => 'add'), 'id' => false, 'onsubmit' => 'return(bb.addNodeSubmit(this));')); ?>
			<?php
				echo $this->Form->input('name', array('class' => 'BbNodeName', 'label' => 'Název'));
				echo $this->Form->input('lat', array('id' => false, 'class' => 'BbNodeLat hidden', 'div' => false, 'label' => false));
				echo $this->Form->input('lng', array('id' => false, 'class' => 'BbNodeLng hidden', 'div' => false, 'label' => false));
			?>
		<?php echo $this->Form->end(__('Submit')); ?>
	</div>
</div>
</body>
</html>
