<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
	<title><?php echo $title_for_layout; ?></title>
	<?php
		echo $this->Html->meta('icon');
		echo $this->Html->css(array('css'));
		echo $this->Html->script(array('jquery-1.9.1.min', 'js'));
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
	<link href='http://fonts.googleapis.com/css?family=Lobster&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	<script>

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-6149741-3']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

	</script>
	
</head>
<body>
	
	<?php echo $this->Session->flash(); ?>
	
	<div id="header">
		<?php
		if ($header = $this->fetch('header')){
			echo $header;
		} else { ?>
			<h1><?php echo __('Kancovy pokusy') ?></h1>
		<?php } ?>
	</div>
	
	<div id="content">
		<?php echo $this->fetch('content'); ?>
	</div>
	
	<div id="footer">
		<h2>Ostatní pokusy</h2>
		<ul>
			<li><?php echo $this->Html->link('Španělská slovíčka', array('controller' => 'words', 'action' => 'index')); ?></li>
			<li><?php echo $this->Html->link('Web na Google Maps API', array('controller' => 'pages', 'action' => 'display', 'map')); ?></li>
		</ul>
		<p>Pro potěchu duše, pro potěchu oka, pro zábavu, ze snahy naučit se Španělštinu, nebo prostě z nudy -
		<?php echo $this->Html->link('Canes.cz', 'http://canes.cz'); ?>
		</p>
	</div>
	<?php echo $this->Js->writeBuffer(); ?>
	<?php echo $this->element('sql_dump');?>
</body>
</html>
