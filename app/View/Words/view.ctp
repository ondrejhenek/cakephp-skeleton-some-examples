<div class="words view">
<h1><?php  echo __('Word'); ?></h1>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($word['Word']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cz'); ?></dt>
		<dd>
			<?php echo h($word['Word']['cz']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('En'); ?></dt>
		<dd>
			<?php echo h($word['Word']['en']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Es'); ?></dt>
		<dd>
			<?php echo h($word['Word']['es']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($word['Word']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($word['Word']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($word['Word']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Word'), array('action' => 'edit', $word['Word']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Word'), array('action' => 'delete', $word['Word']['id']), null, __('Are you sure you want to delete # %s?', $word['Word']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Words'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Word'), array('action' => 'add')); ?> </li>
	</ul>
</div>
