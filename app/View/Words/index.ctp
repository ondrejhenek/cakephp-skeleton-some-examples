<?php
	$this->Paginator->options(array(
		'update' => '#content',
	));
?>

<?php $this->start('header'); ?>
	<h1>Slovíčka</h1>
<?php $this->end(); ?>


<div class="words index">
	
	<table cellpadding="0" cellspacing="0">
	<tr>
		<th><?php echo $this->Paginator->sort('cz', 'Český výraz'); ?></th>
		<th></th>
<!--		<th><?php // echo $this->Paginator->sort('en', 'Anglický výraz'); ?></th>-->
		<th><?php echo $this->Paginator->sort('es', 'Španělský výraz'); ?></th>
		<th><?php echo $this->Paginator->sort('status'); ?></th>
<!--		<th><?php echo $this->Paginator->sort('created'); ?></th>-->
<!--		<th><?php echo $this->Paginator->sort('modified'); ?></th>-->
		<th class="actions"></th>
	</tr>
	<?php
	foreach ($words as $word): ?>
	<tr>
		<td class="word cz"><?php echo h($word['Word']['cz']); ?></td>
		<td class="exchange"><?php echo $this->Js->link('<>', array('action' => 'exchange', $word['Word']['id']), array('update' => '#content')); ?></td>
<!--		<td class="word en"><?php // echo h($word['Word']['en']); ?></td>-->
		<td class="word es"><?php echo h($word['Word']['es']); ?></td>
		<td class="status">
			<?php echo $this->Js->link('<div></div>', array('action' => 'statusup', $word['Word']['id']), array('class' => 'status'.$word['Word']['status'], 'update' => '#content', 'escape' => false)); ?>
		</td>
<!--		<td class="created"><?php
			echo $this->Time->timeAgoInWords($word['Word']['created'], array(
				'end' => '+100 years',
				'accuracy' => array(
					'month' => 'month',
					'week' => 'week',
					'hour' => 'hour',
				),
			));
			?>&nbsp;</td>-->
<!--		<td class="modified"><?php
			echo $this->Time->timeAgoInWords($word['Word']['modified'], array(
				'end' => '+100 years',
				'accuracy' => array(
					'month' => 'month',
					'week' => 'week',
					'hour' => 'hour',
				),
			));
			?>&nbsp;</td>-->
		<td class="actions">
			<?php echo $this->Js->link('', array('action' => 'edit', $word['Word']['id']), array('class' => 'button edit', 'update' => '#content')); ?>
			<?php echo $this->Js->link('', array('action' => 'delete', $word['Word']['id']), array('class' => 'button delete', 'update' => '#content', 'confirm' => __('Are you sure you want to delete #'.$word['Word']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	
	<?php echo $this->Form->create('Word', array('url' => array('controller' => 'words', 'action' => 'add'), 'class' => 'ajaxForm')); ?>
	<table cellpadding="0" cellspacing="0">
		<tr>
			<td class="word cz">
				<?php echo $this->Form->input('cz', array('label' => false, 'div' => false)); ?>
			</td>
			<td class="exchange">
			</td>
<!--			<td class="word en">
				<?php // echo $this->Form->input('en', array('label' => false, 'div' => false)); ?>
			</td>-->
			<td class="word es">
				<?php echo $this->Form->input('es', array('label' => false, 'div' => false)); ?>
			</td>
			<td class="status">
				<?php echo $this->Form->input('status', array('label' => false, 'div' => false)); ?>
			</td>
			<td class="actions">
				<?php echo $this->Js->submit('Uložit', array(
					'url' => array('controller' => 'words', 'action' => 'add'),
					'update' => '#content',
				)); ?>
			</td>
		</tr>
	</table>
	<?php
		
		echo $this->Form->end();
	?>

	<div class="paging">
		<p>
		<?php
			echo $this->Paginator->counter(array(
				'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
			));
		?>
		</p>
		
		<?php
			echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
			echo $this->Paginator->numbers(array('separator' => ''));
			echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
		?>
	</div>
</div>
	
	<script>// $(document).ready(function(){initForms('ajaxForm')}) </script>