<?php
class MenuHelper extends AppHelper {
    var $helpers = array('Html');

    public function makeMenu($items = array(), $activeClass = 'active'){

        $this->tags['ul'] = '<ul>%s</ul>';
        $this->tags['li'] = '<li%s>%s</li>'; 
        $out = array();         
        foreach ($items as $title => $props) 
        {
            if (isset($props['path'])){
                $path = $props['path'];
            } else {
                $path = $props;
            }
            
            $classes = array();
            if (isset($props['submenu'])){
                $classes = array('class' => 'hasSubmenu');
            }
                 
            if($this->url($path) == $this->here){
                $out[] = '<li class="active">'. $this->Html->link($title, $path, $classes); 
            } else { 
                $out[] = '<li>'. $this->Html->link($title, $path, $classes); 
            }
            
            if (isset($props['submenu'])){
                $out[] = '<ul class="submenu">';
                foreach ($props['submenu'] as $subtitle => $subpath){
                    $out[] = '<li>'. $this->Html->link($subtitle, $subpath) .'</li>';
                }
                $out[] = '</ul>'; 
            }
            
            $out[] = '</li>';
        } 
        $tmp = join("\n", $out); 
        return $this->output($tmp); 
    } 

}