<div class="bbNodes index">
	<h1><?php echo __('Bb Nodes'); ?></h1>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('lat'); ?></th>
			<th><?php echo $this->Paginator->sort('lng'); ?></th>
			<th><?php echo $this->Paginator->sort('text'); ?></th>
			<th><?php echo $this->Paginator->sort('status'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
	foreach ($bbNodes as $bbNode): ?>
	<tr>
		<td><?php echo h($bbNode['BbNode']['id']); ?>&nbsp;</td>
		<td><?php echo h($bbNode['BbNode']['name']); ?>&nbsp;</td>
		<td><?php echo h($bbNode['BbNode']['lat']); ?>&nbsp;</td>
		<td><?php echo h($bbNode['BbNode']['lng']); ?>&nbsp;</td>
		<td><?php echo h($bbNode['BbNode']['text']); ?>&nbsp;</td>
		<td><?php echo h($bbNode['BbNode']['status']); ?>&nbsp;</td>
		<td><?php echo h($bbNode['BbNode']['created']); ?>&nbsp;</td>
		<td><?php echo h($bbNode['BbNode']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $bbNode['BbNode']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $bbNode['BbNode']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $bbNode['BbNode']['id']), null, __('Are you sure you want to delete # %s?', $bbNode['BbNode']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>

	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New'), array('action' => 'add')); ?></li>
	</ul>
</div>
