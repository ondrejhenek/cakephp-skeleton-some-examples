<div class="bbNodes form">
<?php echo $this->Form->create('BbNode'); ?>
	<?php echo __('Add Bb Node'); ?>	<?php
		echo $this->Form->input('name');
		echo $this->Form->input('lat');
		echo $this->Form->input('lng');
		echo $this->Form->input('text');
		echo $this->Form->input('status');
	?>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List'), array('action' => 'index')); ?></li>
	</ul>
</div>
