<?php
App::uses('AppModel', 'Model');
/**
 * Word Model
 *
 */
class Word extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'cz' => array(
			'notempty' => array(
				'rule' => array('notempty'),
//				'message' => 'Your custom message here',
//				'allowEmpty' => false,
//				'required' => false,
//				'last' => false, // Stop validation after this rule
//				'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
//		'en' => array(
//			'notempty' => array(
//				'rule' => array('notempty'),
//				'message' => 'Your custom message here',
//				'allowEmpty' => false,
//				'required' => false,
//				'last' => false, // Stop validation after this rule
//				'on' => 'create', // Limit validation to 'create' or 'update' operations
//			),
//		),
		'es' => array(
			'notempty' => array(
				'rule' => array('notempty'),
//				'message' => 'Your custom message here',
//				'allowEmpty' => false,
//				'required' => false,
//				'last' => false, // Stop validation after this rule
//				'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
//		'status' => array(
//			'numeric' => array(
//				'rule' => array('numeric'),
////				'message' => 'Your custom message here',
////				'allowEmpty' => false,
////				'required' => false,
////				'last' => false, // Stop validation after this rule
////				'on' => 'create', // Limit validation to 'create' or 'update' operations
//			),
//		),
	);
	
	public function beforeSave($options = array()){
		parent::beforeSave();
		if (isset($this->data['Word']['status']) && empty($this->data['Word']['status'])){
			unset($this->data['Word']['status']);
		}
	}
}
