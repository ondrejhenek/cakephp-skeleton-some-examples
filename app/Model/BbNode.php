<?php
App::uses('AppModel', 'Model');
/**
 * BbNode Model
 *
 */
class BbNode extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notempty' => array(
				'rule' => array('notempty'),
//				'message' => 'Your custom message here',
//				'allowEmpty' => false,
//				'required' => false,
//				'last' => false, // Stop validation after this rule
//				'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'lat' => array(
			'notempty' => array(
				'rule' => array('notempty'),
//				'message' => 'Your custom message here',
//				'allowEmpty' => false,
//				'required' => false,
//				'last' => false, // Stop validation after this rule
//				'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'lng' => array(
			'notempty' => array(
				'rule' => array('notempty'),
//				'message' => 'Your custom message here',
//				'allowEmpty' => false,
//				'required' => false,
//				'last' => false, // Stop validation after this rule
//				'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'text' => array(
			'notempty' => array(
				'rule' => array('notempty'),
//				'message' => 'Your custom message here',
//				'allowEmpty' => false,
//				'required' => false,
//				'last' => false, // Stop validation after this rule
//				'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'status' => array(
			'numeric' => array(
				'rule' => array('numeric'),
//				'message' => 'Your custom message here',
//				'allowEmpty' => false,
//				'required' => false,
//				'last' => false, // Stop validation after this rule
//				'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
}
