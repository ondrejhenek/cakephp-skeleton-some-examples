function initialize() {
	var mapOptions = {
		center: new google.maps.LatLng(49.195102, 16.608528),
		zoom: 15,
		mapTypeId: google.maps.MapTypeId.HYBRID,
		disableDefaultUI: true
	};
	var map = new google.maps.Map(document.querySelector("#map_canvas"), mapOptions);
}