if (location.hostname == 'localhost') var domain = 'http://localhost/pokus/';
    else var domain = 'http://'+ location.hostname +'/';

function alrt(data, success){
	/* zobrazit horní lištu ale FIXED
	 * při úspěchu po 5 vteřinách zavřít
	 * při neúspěchu nechat zobrazenou
	 * ošetřit více úspěchů/neúspěchů a taky timer zobrazení
	 */
	alert(data);
}

function initForms(formClass){
	return;
	$('.' + formClass).submit(function(){
		var form = $(this);
		var button = form.find('.submit input');
		var inputs = form.find('input');
		var data = array;
		for (var i = 0; i < inputs.length; i++){
			if (inputs[i].name.length){
//				eval(inputs[i].name) inputs[i].value;
			}
		}
		console.log(data);
		load(button);
		$.ajax({
			type: 'POST',
			url: form.attr('action'),
			data: data,
			success: function(data){
				var result = processAjaxSuccess(data);
				unload(button, result);
//				if (result){
//					setTimeout(function(){
//						$('#HowdyIndexForm .wrap').animate({'opacity': 0}, 1000, function(){$(this).css('visibility', 'hidden');});
//					}, 1000);
//				}
			},
			error: function(jqXHR, textStatus, errorThrown){
				alrt(textStatus+ ' : ' +errorThrown+ ' -> '+ jqXHR, false);
				unload(button, false);
			},
			dataType: 'json'
		});
		return false;
	});
}

/* ************************** LOADING FUNCTIONS ************************** */
function load(elem){
	$(elem).attr({disabled: 'disabled'}).addClass('loadingProgress loadingItem');
}
function unload(elem, result){
	var loadClass = result ? 'loadingSuccess' : 'loadingFailure';
	$(elem).removeAttr('disabled').addClass(loadClass).removeClass('loadingItem');
	setTimeout(function(){
		timeoutUnload(elem, loadClass);
	}, 1000);
}
function timeoutUnload(elem, loadClass){
	$(elem).removeClass('loadingProgress '+ loadClass);
}
function processAjaxSuccess(data){
	if (data == null || data.error == null){
		alrt('Oops! Požadavek jsme nebyli schopni zpracovat. :( \nNapište nám prosím, že tu máme šotka!', false);
		return false;
	} else {
		if (data.error){
			alrt(data.error, false);
			return false;
		}
	}
	return true;
}

/*$(document).ready(function(){
	$('.word').bind('mousedown', function(e) {
			$(this).addClass('hit');
		});

	$('.word').bind('mouseup', function(e) {
		$(this).removeClass('hit');
	});
});*/

/*
.word.hit {
	background: url('../img/test-error-icon.png') left top no-repeat;
	-webkit-animation: hit .15s linear 5;
}

@-webkit-keyframes hit{
  0%   { background-size: 10%;}
  10%  { background-size: 9.5%; }
  30%  { background-size: 9.7%; }
  50%  { background-size: 10.0%;}
  60%  { background-size: 9.8%; }
  70%  { background-size: 10.0%;}
  80%  { background-size: 9.9%; }
  100% { background-size: 10.0%;}
}
*/