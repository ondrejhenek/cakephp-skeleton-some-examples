if (location.hostname == 'localhost') var domain = 'http://localhost/pokus/';
    else var domain = 'http://'+ location.hostname +'/';

function alrt(msg, status){
	alert(msg);
}

var bb = {
	mapOptions: {
		center: new google.maps.LatLng(49.195102, 16.608528),
		zoom: 15,
		mapTypeId: google.maps.MapTypeId.HYBRID,
		disableDefaultUI: true
	},
	map: {},
	init: function(){
		var self = this;
		this.map = new google.maps.Map(document.getElementById("map_canvas"), this.mapOptions);
		google.maps.event.addListener(this.map, 'click', function(event){
			self.addNode(event.latLng);
		});
	},
	getNodes: function(param){
//		$.ajax({
//			url: domain +'bbnodes/get',
//			dataType: 'JSON',
//			success: function(data){processXML(data, what)},    
//			error: function(jqXHR, textStatus, errorThrown){
//				alrt('Ajaj, stala se chybička: '+textStatus+' '+errorThrown);
//			} 
//		})
	},
	newMarker: false, // objekt, do kterého budu ukládat nově vytvořený marker
	newInfoBox: false, // objekt, do kterého budu ukládat nově vytvořený infoBox
	prepareInfoBox: function(shadowContent){
		var content = $('<div class="content">').html($(shadowContent).html());
		// následující kód není ideální. Radši bych si měl HTML strutkuru vytvořit v dokukmentu a pak ji jen kopírovat...
		var arrow = $('<div class="infoBoxArrows"><div class="infoBoxArrowInner"></div><div class="infoBoxArrowOuter"></div></div>');
		return $('<div>').append(content, arrow);
	},
	// metoda pro vytváření nových míst a jejich ukládání do databáze
	addNode: function(latLng){
		var self = this;
		var newNodeFormId = 'newNodeForm';
		// pokud mám na mapě už nový marker, odstraním ho, ať se tam neplete
//		if (self.newMarker) self.newMarker.setMap(null);
//		// připíchnu nový marker na mapu
//		self.newMarker = new google.maps.Marker({
//			position: latLng,
//			map: this.map
//		});
		
		// zkopíruju formulář do nového elementu
		var newNodeFormWrap = self.prepareInfoBox('#newNodeFormShadow');
		newNodeFormWrap.find('form').attr('id', newNodeFormId);
		
		// vytvořím bublinu pro přidání nového místa
//		var newInfoWindow = new google.maps.InfoWindow({
//			content: newNodeFormWrap.html(),
//			size: new google.maps.Size(50,50)
//		});
//		newInfoWindow.open(self.map, self.newMarker);
		if (self.newInfoBox) self.newInfoBox.close();
		self.newInfoBox = new InfoBox({
			content: newNodeFormWrap.html(),
			alignBottom: true,
			pixelOffset: new google.maps.Size(-140, -10),
			boxClass: "newInfoBox infoBox",
			boxStyle: {
				width: "280px"
			},
			closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
			infoBoxClearance: new google.maps.Size(5, 5)
        });
		self.newInfoBox.open(self.map);
		self.newInfoBox.setPosition(latLng);
		
			// poté, co se načte DOM infoBoxu (ano, chvíli to trvá) ...
		google.maps.event.addListener(self.newInfoBox, 'domready', function(){
			var newNodeForm = $('#'+ newNodeFormId);
			var latInput = newNodeForm.find('.BbNodeLat');
			$(latInput).attr('value', latLng.lat());
			var lngInput = newNodeForm.find('.BbNodeLng');
			$(lngInput).attr('value', latLng.lng());
			var nameInput = newNodeForm.find('.BbNodeName');
				// chvilku trvá, než můžu zařídit focus, možná by šlo ošetřit callbackem, ale kterým?
			setTimeout(function(){
				$(nameInput).focus();
			}, 10);
		});
		
		

		// musím si redefinovat newNodeForm, protože vložení do infoWindow to zmastí
//		newNodeForm = $('#newNodeForm');
//		var newNodeFormSubmit = newNodeForm.find('input[type=submit]');
//		console.log(newNodeForm);
//		newNodeForm.submit(function(){
//			load(newNodeFormSubmit);
//			$.ajax({
//				url: domain +'bbnodes/get',
//				dataType: 'JSON',
//				success: function(data){
//					unload(newNodeFormSubmit, processAjaxSuccess(data));
//				},
//				error: function(jqXHR, textStatus, errorThrown){
//					alrt('Ups, stala se chybička!\n' +errorThrown+ ' - '+ jqXHR.responseText);
//					unload(newNodeFormSubmit);
//				}
//			});
//			return false;
//		});
	},
	addNodeSubmit: function(form){
		form = $(form);
		
		var newNodeFormSubmit = form.find('input[type=submit]');
		console.log(form);
		load(newNodeFormSubmit);
		$.ajax({
			url: domain +'bbnodes/add',
			dataType: 'JSON',
			success: function(data){
				unload(newNodeFormSubmit, processAjaxSuccess(data));
			},
			error: function(jqXHR, textStatus, errorThrown){
				alrt('Ups, stala se chybička!\n' +errorThrown+ ' - '+ jqXHR.responseText);
				unload(newNodeFormSubmit);
			}
		});
		return false;
	}
}

function initialize() {
	bb.init();
}

function load(elem){
	$(elem).attr({disabled: 'disabled'}).addClass('loadingProgress loadingItem');
}
function unload(elem, result){
	var loadClass = result ? 'loadingSuccess' : 'loadingFailure';
	$(elem).removeAttr('disabled').addClass(loadClass).removeClass('loadingItem');
	setTimeout(function(){
		timeoutUnload(elem, loadClass);
	}, 1000);
}
function timeoutUnload(elem, loadClass){
	$(elem).removeClass('loadingProgress '+ loadClass);
}
function processAjaxSuccess(data){
	if (data == null || data.error == null || data.text == null){
		alrt('Oops! Požadavek jsme nebyli schopni zpracovat. :( \nNapište nám prosím, že tu máme šotka!');
		return false;
	} else {
		if (data.error){
			alrt(data.text);
			return false;
		}
	}
	return true;
}