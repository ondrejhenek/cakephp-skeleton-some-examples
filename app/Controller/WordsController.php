<?php
App::uses('AppController', 'Controller');
/**
 * Words Controller
 *
 * @property Word $Word
 */
class WordsController extends AppController {
	public $components = array('RequestHandler');
	public $helpers = array('Js');
	
	public function index() {
		$this->Word->recursive = 0;
		$this->paginate = array(
			'limit' => 100,
			'order' => 'created DESC',
		);
		$this->set('words', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->Word->id = $id;
		if (!$this->Word->exists()) {
			throw new NotFoundException(__('Chybné ID, položka neexistuje.'));
		}
		$this->set('word', $this->Word->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Word->create();
			if ($this->Word->save($this->request->data)) {
				$this->Session->setFlash(__('Položka byla úspěšně uložena.'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Položka nemohla být uložena. Zkuste to prosím znovu.'), 'default', array('class' => 'failure'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Word->id = $id;
		if (!$this->Word->exists()) {
			throw new NotFoundException(__('Chybné ID, položka neexistuje.'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Word->save($this->request->data)) {
				$this->Session->setFlash(__('Položka byla úspěšně uložena.'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Položka nemohla být uložena. Zkuste to prosím znovu.'), 'default', array('class' => 'failure'));
			}
		} else {
			$this->request->data = $this->Word->read(null, $id);
		}
	}
	
	
	public function exchange($id = null) {
		$this->Word->id = $id;
		if (!$this->Word->exists()) {
			throw new NotFoundException(__('Chybné ID, položka neexistuje.'));
		}
		$words = $this->Word->read(array('cz', 'es'), $id);
		$tmp = $words['Word']['cz'];
		$words['Word']['cz'] = $words['Word']['es'];
		$words['Word']['es'] = $tmp;
		if ($this->Word->save($words)) {
			$this->Session->setFlash(__('Položka byla úspěšně uložena.'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		} else {
			$this->Session->setFlash(__('Položka nemohla být uložena. Zkuste to prosím znovu.'), 'default', array('class' => 'failure'));
		}
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
//		if (!$this->request->is('post')) {
//			throw new MethodNotAllowedException();
//		}
		$this->Word->id = $id;
		if (!$this->Word->exists()) {
			throw new NotFoundException(__('Chybné ID, položka neexistuje.'));
		}
		if ($this->Word->delete()) {
			$this->Session->setFlash(__('Položka byla úspěšně odstraněna.'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Položka nemohla být odstraněna. Zkuste to prosím znovu.'), 'default', array('class' => 'failure'));
		$this->redirect(array('action' => 'index'));
	}
	
	public function statusup($id = null) {
		$this->Word->id = $id;
		if (!$this->Word->exists()) {
			throw new NotFoundException(__('Chybné ID, položka neexistuje.'));
		}
		if ($this->Word->updateAll(array('Word.status' => 'Word.status+1'), array('Word.id' => $id))) {
			$this->Session->setFlash(__('Položka byla úspěšně uložena.'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Položka nemohla být uložena. Zkuste to prosím znovu.'), 'default', array('class' => 'failure'));
	}
	
	public function rand($entries = 15) {
		$words = $this->Word->find('all', array(
			'limit' => $entries,
			'order' => 'RAND()',
		));
		$this->set('words', $words);
	}
}
