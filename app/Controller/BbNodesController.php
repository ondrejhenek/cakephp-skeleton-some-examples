<?php
App::uses('AppController', 'Controller');
/**
 * BbNodes Controller
 *
 * @property BbNode $BbNode
 */
class BbNodesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->BbNode->recursive = 0;
		$this->set('bbNodes', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->BbNode->id = $id;
		if (!$this->BbNode->exists()) {
			throw new NotFoundException(__('Wrong ID, item does not exist.'), 'default', array('class' => 'failure'));
		}
		$this->set('bbNode', $this->BbNode->read(null, $id));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->BbNode->create();
			if ($this->BbNode->save($this->request->data)) {
				$this->Session->setFlash(__('Item was succesfully saved.'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Item could not be saved. Please, try again later.'), 'default', array('class' => 'failure'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->BbNode->id = $id;
		if (!$this->BbNode->exists()) {
			throw new NotFoundException(__('Wrong ID, item doesn not exist.'), 'default', array('class' => 'failure'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->BbNode->save($this->request->data)) {
				$this->Session->setFlash(__('Item was succesfully saved.'), 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Item could not be saved. Please, try again later.'), 'default', array('class' => 'failure'));
			}
		} else {
			$this->request->data = $this->BbNode->read(null, $id);
		}
	}

/**
 * delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->BbNode->id = $id;
		if (!$this->BbNode->exists()) {
			throw new NotFoundException(__('Wrong ID, item does not exist.'), 'default', array('class' => 'failure'));
		}
		if ($this->BbNode->delete()) {
			$this->Session->setFlash(__('Item was successfully deleted.'), 'default', array('class' => 'success'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Item could not be deleted. Please, try again later.'), 'default', array('class' => 'failure'));
		$this->redirect(array('action' => 'index'));
	}
}
