<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
	<title><?php echo $title_for_layout; ?></title>
	<?php
		echo $this->Html->meta('icon');
		echo $this->Html->css(array('css'));
		echo $this->Html->script(array('js', 'jquery-1.8.3.min'));
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
//		if (!isset($admin) || !$admin){
//			echo '<style>.old_style_actions, .actions{display: none;}</style>';
//		}
	?>
<!--	<link rel="apple-touch-icon" href="/img/apple-touch-icon.png">-->
	
	<script>
// analytics?! :)
	</script>
	
</head>
<body>
	
	<?php echo $this->Session->flash(); ?>
	
	<div id="header">
		<div class="logo">
			<?php echo $this->Html->link(__('CakePHP: the rapid development php framework'), 'http://cakephp.org'); ?>
		</div>
	</div>
	
	<div id="content">
		<?php echo $this->fetch('content'); ?>
	</div>
	
	<div id="footer">
		<?php echo $this->Html->link(
				$this->Html->image('cake.power.gif', array('alt' => __('CakePHP: the rapid development php framework'), 'border' => '0')),
				'http://www.cakephp.org/',
				array('target' => '_blank', 'escape' => false)
			);
		?>
	</div>
	
	<?php echo $this->element('sql_dump'); ?>
</body>
</html>
